ruleset sensor_profile{
    meta {
        name "Sensor Profile"
        description << Profile attributes >>
        author "Jacob Livingston"

        provides threshold, phone_number
        shares threshold, phone_number
    }

    global {
        threshold = function(){return ent:threshold}
        phone_number = function(){return ent:phone}
    }

    rule profile_update {
        select when sensor profile_updated
        fired{
            ent:threshold := event:attr("threshold").klog("Updating threshold to: ");
            ent:phone := event:attr("phone").klog("Updating phone number to: ");
        }
    }
}