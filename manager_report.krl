ruleset manager_report {
    meta {
      name "Manager reporter"
      description << Generates a report based off all the sensors that are in it's collection >>
      author "Jacob Livingston"
      use module io.picolabs.wrangler alias wrangler
      use module io.picolabs.subscription alias Subscriptions
      provides thresh, current, report
      shares thresh, current, report
    }
    
    global {
      report_single = function(id) {
        total =ent:sensors_sent.get(id).length()
        received = ent:sensors_received.get(id).length()
        return {"id":id,
                "report":{"temperature_sensors":total,
                          "respondng":received,
                          "temperatures":ent:reports.get(id)
                }}
      }
      
      report = function() {
        return ent:reports.filter(function(v,k){v > (ent:report_id-5)}).map(function(v,k){report_single(k)})
      }
    }
    
    rule init_report {
      select when sensor report_init
      
      always {
        ent:report_id := (ent:report_id + 1).klog("Report id: ") ;
        ent:reports{ent:report_id} := []
        ent:sensors_sent{ent:report_id} := []
        ent:sensors_received{ent:report_id} := []
  
        raise sensor event "send_init_report"
          attributes {"id": ent:report_id}
      }
      
    }
    
    rule send_init {
      select when sensor send_init_report
      foreach Subscriptions:established("Rx_role","sensor") setting(subscription)
        pre {
          a = subscription{"Tx"}.klog("Requesting Report from: ")
          c = ent:sensors_sent.get(event:attr("id")).append([a])
          
        }
        event:send(
              { "eci": subscription{"Tx"}, "eid": "1234",
                  "domain": "sensor", "type": "reporter",
                  "attrs": {"id": event:attr("id" )}
              }
            )
        fired {
            ent:sensors_sent{event:attr("id")} := c
  
        }
    }
    
    rule receive_report {
      select when sensor receive_report
      pre {
        id = event:attr("id").klog("Received: ")
        rx = event:attr("rx")
        temp = event:attr("temp")
        a = klog("Receieved report for: " + id + " from: " + rx + "  {"+temp+"°F}")
      }
      
      always {
        b = ent:reports.get(id).append([{"id":rx,"temp":temp}]).klog("Report: ")
        ent:reports{id} := b
  
        c = ent:sensors_received.get(id).append([rx])
        ent:sensors_received{id} := c
        a = ent:reports.klog()
  
      }
    }
    
    rule reset_reports {
      select when sensor reset_report
      send_directive("Before reset", {"reports":ent:reports,"received":ent:sensors_received,"sent":ent:sensors_sent,"id":ent:report_id})
  
      fired {
        ent:reports := {}
        ent:sensors_received := {}
        ent:sensors_sent := {}
        ent:report_id := 0
  
      }
  
    }
  }