ruleset wovyn_base {
    meta {
        name "Wovyn Base"
        description << Catch Wovyn Heartbeat >>
        author "Jacob Livingston"
        use module sensor_profile alias profile
        use module io.picolabs.subscription alias Subscriptions
        use module setup_subscription alias sub

        provides thresh, current
        shares thresh, current
    }
    
    global{
            thresh = function(){return ent:highTemp}
            current = function(){return ent:temp}

    }

    rule hearbeat {
        select when wovyn heartbeat
        pre {
            
        }
        send_directive("say",{"msg":"Received Temperature"})
        fired {
            ent:temp := event:attr("genericThing").get(["data","temperature"]).head().get(["temperatureF"]).klog("Data: ")
            raise wovyn event "new_temperature_reading"
                attributes {"temperature": ent:temp,
                            "timestamp":time:now()}
        }
    }

    rule find_high_temps {
        select when wovyn new_temperature_reading
        send_directive("say",{"msg":msg})
        
        fired {
            highTemp =profile:threshold().as("Number").klog("Thresh: ")
            temp = event:attr("temperature").as("Number").klog("Attributes: " )
            isHigh = ((temp > highTemp)).klog(temp + " is higher than "+highTemp+" degrees: ") 
            msg = isHigh=>"Too Hot"|"Just Right"
            
            raise wovyn event "threshold_notification" 
                attributes {"temperature": temp,
                            "timestamp":time:now()} if isHigh
            
        }
        
    }

    rule threshold_notify {
        select when wovyn threshold_notification
        foreach Subscriptions:established("Rx_role","manager") setting(subscription)
          pre {
              temp = event:attr("temperature")
              date = event:attr("timestamp")
              a = klog(date + "  :::   Notifying of high temp: " + temp)
          
          }   
          event:send(
                    { "eci": subscription{"Tx"}, "eid": "1234",
                        "domain": "sensor", "type": "threshold_violation",
                        "attrs": {
                            "temp":temp,
                            "date":date
                                  
                          }}
                  )
    }
    
    rule report {
      select when sensor reporter
      fired {
        id = event:attr("id").klog("Reporting on: ")
        raise sensor event "send_report"
          attributes{"id":id}
      }
    }
    
    rule send_report {
      select when sensor send_report
        foreach Subscriptions:established("Rx_role","manager") setting(subscription)
          pre {
              id = event:attr("id")
              rx =sub:wellKnown().get("id").klog("Wellknown: ")
              temp = current().klog("Current: ")
          
          }   
          event:send(
                    { "eci": subscription{"Tx"}, "eid": "1234",
                        "domain": "sensor", "type": "receive_report",
                        "attrs": {
                            "temp":temp,
                            "id":id,
                            "rx":rx
                                  
                          }}
                  )
    }
}