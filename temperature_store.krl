ruleset temperature_store {
    meta {
        name "Temperature Store"
        description << Catch Wovyn Heartbeat >>
        author "Jacob Livingston"

        provides temperatures, threshold_violations, inrange_temperatures
        shares temperatures, threshold_violations, inrange_temperatures
    }

    global {
        temperatures = function(){return ent:temperatures.values()}
        threshold_violations = function(){return ent:temperatureViolations.values()}
        inrange_temperatures = function(){return ent:temperatures.values().difference(ent:temperatureViolations.values())}
        myRid = function() { meta:rid }

    }

    rule collect_temperatures {
        select when wovyn new_temperature_reading
        fired {
            temp = event:attr("temperature").klog("Temperature: " );
            timestamp = event:attr("timestamp").klog("Timestamp: ");
            ent:temperatures{timestamp} := temp;
            b = temperatures().klog("Temps: ")
            c = threshold_violations().klog("Violations: ")
            a = inrange_temperatures().klog("Inrange")
            d = myRid().klog()
        }
       
    }

    rule collect_threshold_violations {
        select when wovyn threshold_notification
        fired {
            temp = event:attr("temperature").klog("Temperature: " );
            timestamp = event:attr("timestamp").klog("Timestamp: ");
            ent:temperatureViolations{timestamp} := temp;
        }
        
    }

    rule clear_temeratures  {
        select when wovyn reading_reset
        fired {
            clear ent:temperatures;
            clear ent:temperatureViolations
            a = ent:temperatures.klog("Cleared? ")
        }
        
    }

    
}