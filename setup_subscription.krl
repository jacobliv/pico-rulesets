ruleset setup_subscription {
    meta {
      use module io.picolabs.wrangler alias wrangler
      use module io.picolabs.subscription alias Subscriptions
  
      
      provides wellKnown
      shares wellKnown
    }
    
    global {
        wellKnown = function(){return Subscriptions:wellKnown_Rx().klog("Wellknown: ")}
    }
  }