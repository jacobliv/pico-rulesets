ruleset manage_sensors{
  meta {
      name "Sensor Manager"
      description << Manages The sesors >>
      author "Jacob Livingston"
      use module io.picolabs.wrangler alias wrangler
      use module io.picolabs.subscription alias Subscriptions

  }

  global {
    showChildren = function() {
      wrangler:children().klog("Children: ")
    }
    send_profile = function(eci) {
      http:post("http://localhost:8080/sky/"+eci+"/1234/sensor/wellKnown",
                body={"threshold":55}
      ) 
    }
    
    get_temp = function(url) {
      http:get(url){"content"}.decode().klog("Temp: ")
    }
    
    get_child_rx = function(eci){
      wrangler:skyQuery( eci , "io.picolabs.subscription", "wellKnown_Rx").get("id").klog("RX: ")
    }
    
  }
  
  rule heartbeat {
    select when wovyn heartbeat
    
    pre {
      name = event:attr("property").get("name").klog("Data: ")
      id = ent:subName.get(name).klog("id: ")

      subs = Subscriptions:established("Id",id)
      temp = event:attr("genericThing").get(["data","temperature"]).head().get(["temperatureF"]).klog("Temp: ")
    }

    always{
      a = length.klog("In Fired: ")
      b = val.klog("Val: ")

      raise sensor event "new_sensor"
              attributes {"name": name} if subs.length() ==0 
      raise sensor event "forward"
          attributes {"id" : id,
                          "genericThing": 
                                {"data":
                                  {"temperature":
                                      [{"temperatureF":temp}]
                                  }}}

    }
    

  }
  
  rule forward {
    select when sensor forward
    foreach Subscriptions:established("Id",event:attr("id")) setting(subscription)
      pre{
        data = event:attr("genericThing").klog("Generic Thing: ")
      }
      event:send(
                  { "eci": subscription{"Tx"}, "eid": "1234",
                      "domain": "wovyn", "type": "heartbeat",
                      "attrs": {
                          "genericThing": data
                                
                        }}
                )
  }


  rule all_temps {
    select when sensor all_temps
    always {
      a = ent:sensors.map(function(key,value){get_temp("http://localhost:8080/sky/cloud/"+key+"/wovyn_base/current")})
      b=a.klog("Result")
    }
  }

  rule store_new_section {
    select when wrangler child_initialized
    
    pre {
      name = event:attr("name").klog("New: ")
      eci = event:attr("eci").klog("Eci: ")
      things = wrangler:children().klog("Children: ")
      rx = get_child_rx(eci)
    }

    always {
      ent:sensors{name}:= eci
      raise sensor event "new_sub"
        attributes {"name":name,
                    "wellKnown_Tx": rx}

    }
  }
  
  rule new_sub{
    select when sensor new_sub
    always {
      name = event:attr("name")
      rx = event:attr("wellKnown_Tx")
      
      raise wrangler event "subscription" 
        attributes { "name" : name,
                     "Rx_role": "sensor",
                     "Tx_role": "manager",
                     "channel_type": "subscription",
                     "wellKnown_Tx" : rx
                   }
    }
  }
  
  rule sub_approved {
    select when wrangler outbound_pending_subscription_approved
    always {
      name = event:attr("name").klog("NAME: ")
      ent:subName{event:attr("name")} := event:attr("Id").klog("Id: ")
      sub = Subscriptions:established("Id",event:attr("Id")).klog("New Sub: ")
      raise sensor event "send_init"
        attributes { "id":event:attr("Id")}
    }
  }
  
  rule send_init_data {
    select when sensor send_init

    foreach Subscriptions:established("Id",event:attr("id")) setting(subscription)
      event:send(
                  { "eci": subscription{"Tx"}, "eid": "1234",
                    "domain": "sensor", "type": "profile_updated",
                    "attrs": {
                      "threshold": ent:threshold,
                      "phone": ent:phone,
                      "name": ent:name,
                      "location": ent:location
                      }}
                )
      always {
        
        ent:threshold :=65
        ent:phone := "630-809-3526"
        ent:name := "Jacob"
        ent:location := "Provo"
        
        }
    
  }

  rule new_sensor{
      select when sensor new_sensor
      fired {
          name = event:attr("name")
          raise wrangler event "child_creation"
            attributes { "name": name, "color": "#ffff00",
                          "rids": ["setup_subscription",
                                    "auto_accept",
                                    "wovyn_base",
                                    "temperature_store",
                                    "sensor_profile",
                                    "io.picolabs.logging"]
                        }.klog("Name: ")
      }
  }
  
  rule delete_subs{
    select when sensor delete_subs
    foreach Subscriptions:established("Id",event:attr("id")) setting(subscription)
      fired {
        a = "Deleting Sub".klog()
        raise wrangler event "subscription_cancellation"
          attributes {"Tx":subscription{"Tx"}}
      }
  }
  
  rule delete_sensor{
    select when sensor delete_sensor
    fired {
      name = event:attr("name").klog("Deleting: ")
      a = ent:subName.klog("ALL NAMES: ")
      id = ent:subName.get(name).klog("Id: ")
      ent:subName := ent:subName.delete(name)
      raise wrangler event "child_deletion"
        attributes {"name": name};
      raise sensor event "delete_subs"
          attributes {"id":id}
    }
  }

rule thresh_violation {
  select when sensor threshold_violation
  
  fired {
    raise sensor event "send_message"
      attributes {
        "message": "Temperature threshold exception: " + event:attr("temp") +
                            "degrees at " + event:attr("date")
      }
  }
}

}
