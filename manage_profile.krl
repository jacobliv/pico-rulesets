ruleset manage_profile {
    meta{
      
    }
    
    rule send {
        select when sensor send_message
        fired {
          ent:phone:="630-809-7062"
          message = event:attr("message")
          raise tiwlio event "send_text"
            attributes {"phone":ent:phone,
                        "message":message}
        }
      }
  }